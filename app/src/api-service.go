package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

func main() {
	fmt.Printf("[%s] Starting API Service...\n", time.Now())
	http.HandleFunc("/", handler)
	fmt.Printf("API Service started on %s\n", os.Args[1])
	log.Fatal(http.ListenAndServe(os.Args[1], nil))
}

type serviceInfo struct {
	Version string
	Host    string
}

func handler(w http.ResponseWriter, r *http.Request) {
	info := new(serviceInfo)
	info.Version = os.Getenv("SOURCE_COMMIT")
	info.Host, _ = os.Hostname()
	data, _ := json.Marshal(info)
	fmt.Fprint(w, string(data[:]))
}